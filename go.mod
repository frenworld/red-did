module gitlab.com/frenworld/red-did

go 1.21.1

require (
	github.com/airbrake/gobrake/v5 v5.6.1
	github.com/bwmarrin/discordgo v0.27.2-0.20240307155122-202785c50b9e
	go.mongodb.org/mongo-driver v1.14.0
)

require (
	github.com/caio/go-tdigest/v4 v4.0.1 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/jonboulle/clockwork v0.4.0 // indirect
	github.com/klauspost/compress v1.17.7 // indirect
	github.com/montanaflynn/stats v0.7.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.2 // indirect
	github.com/xdg-go/stringprep v1.0.4 // indirect
	github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a // indirect
	golang.org/x/net v0.22.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)

require (
	github.com/gorilla/websocket v1.5.1 // indirect
	github.com/joho/godotenv v1.5.1
	golang.org/x/crypto v0.21.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
)
